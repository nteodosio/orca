<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="commands_controlling_orca" xml:lang="pt-BR">
  <info>
    <link type="next" xref="commands_where_am_i"/>
    <link type="guide" xref="commands#getting_started"/>
    <title type="sort">1. Controlando e aprendendo a usar o Orca</title>
    <title type="link">Controlando e aprendendo a usar o Orca</title>
    <desc>Comandos para interagir com o <app>Orca</app></desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Atribuição Compartilhada Igual 3.0 — Creative Commons</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Conte</mal:name>
      <mal:email>alente.alemao@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Vilmar Estácio De Souza</mal:name>
      <mal:email>vilmar@informal.com.br</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Controlando e aprendendo a usar o Orca</title>
  <section id="controlling">
    <title>Comandos para controlar o <app>Orca</app></title>
    <p>Os comandos a seguir podem ser usados para entrar nas preferências do <app>Orca</app>, sair do <app>Orca</app> e ignorar comandos do <app>Orca</app> para evitar conflito de atalhos com o aplicativo que está sendo acessado.</p>
    <list>
      <item>
        <p>Ative ou desative o <app>Orca</app> no GNOME: <keyseq><key>Super</key><key>Alt</key><key>S</key></keyseq>.</p>
        <note style="tip">
           <p>Se você estiver usando o <app>Orca</app> em outro ambiente de desktop no qual não há comando para ativar e desativá-lo, você pode achar útil o comando para sair do <app>Orca</app>. Este comando não tem atalho de teclado definido por padrão. Consulte <link xref="howto_key_bindings">Associações de teclas</link> para obter informações sobre como vincular comandos a um atalho de teclado.</p>
        </note>
      </item>
      <item>
        <p><link xref="preferences">Preferências do Orca</link> Diálogo: <keyseq><key>Tecla Modificadora do Orca</key><key>barra de espaço</key></keyseq>.</p>
      </item>
      <item>
        <p>Preferências do Orca para o aplicativo atual: <keyseq><key>Ctrl</key><key>Tecla Modificadora do Orca</key><key>barra de espaço</key></keyseq>.</p>
      </item>
      <item>
        <p>Envia o próximo comando para o aplicativo atual: <keyseq><key>Tecla Modificadora do Orca</key><key>BackSpace</key></keyseq></p>
      </item>
    </list>
  </section>
  <section id="learning">
    <title>Comandos para aprender a usar o <app>Orca</app></title>
    <p>No modo de aprendizado, o <app>Orca</app> anunciará cada tecla pressionada juntamente com o comando do <app>Orca</app> associado àquela tecla. Neste modo, você também pode obter uma lista de teclas de atalho contendo todos os comandos do <app>Orca</app> que você pode usar.</p>
    <list>
      <item>
        <p>Entra no Modo de Aprendizado: <keyseq><key>Tecla Modificadora do Orca</key><key>H</key></keyseq></p>
      </item>
      <item>
        <p>Sai do Modo de Aprendizado: <keyseq><key>Esc</key></keyseq></p>
      </item>
    </list>
  </section>
</page>
