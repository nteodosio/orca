<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="howto_mouse_review" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="index#reviewing"/>
    <link type="next" xref="howto_notifications"/>
    <title type="sort">4. Revisão por Mouse</title>
    <desc>Usando o ponteiro para explorar a tela</desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Atribuição Compartilhada Igual 3.0 — Creative Commons</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Conte</mal:name>
      <mal:email>alente.alemao@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Vilmar Estácio De Souza</mal:name>
      <mal:email>vilmar@informal.com.br</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Revisão por Mouse</title>
  <p>O recurso Revisão por Mouse do <app>Orca</app> faz com que o leitor apresente o objeto sob o ponteiro do mouse. Ao contrário do recurso <link xref="howto_flat_review">Revisão Plana</link>, a Revisão por Mouse não se limita à janela ativa; em vez disso, o <app>Orca</app> tentará identificar qual objeto acessível, se houver, está visualmente sob o ponteiro enquanto você move o mouse. Se você moveu o ponteiro sobre um objeto acessível com informações a serem apresentadas, o <app>Orca</app> apresentará esse objeto e suas informações para você.</p>
  <p>Como habilitar a Revisão por Mouse faz com que o <app>Orca</app> escute e processe todas as alterações na posição do ponteiro do mouse, esse recurso está desabilitado por padrão. Para tê-lo sempre ativado, marque a caixa de seleção <gui>Falar o objeto sob o mouse</gui> encontrada na <link xref="preferences_general">Página geral da caixa de diálogo Preferências do <app>Orca</app></link>. Além disso, você encontrará um comando Sem atalho definido chamado <gui>Alternar modo de revisão do mouse</gui> na <link xref="preferences_key_bindings">Página de Atalhos do Teclado</link> dessa mesma caixa de diálogo. Ao vincular este comando, conforme descrito na <link xref="howto_key_bindings">Introdução aos Atalhos de Teclado</link>, você pode habilitar e desabilitar a Revisão por Mouse, conforme necessário.</p>
  <note style="tip">
    <title>As configurações padrão e as combinações de teclas são independentes umas das outras</title>
    <p>Observe que você pode ou não escolher para ter a revisão do mouse sempre habilitada e ainda assim alterná-la entre ligada e desligada por meio do teclado usando o <gui>modo de alternância da revisão do mouse</gui>. Não é necessário habilitá-la com o objetivo de alternar, porque as definições e as associações de teclas são independentes entre sí.</p>
  </note>
</page>
