<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="preferences_web" xml:lang="pt-BR">
  <info>
    <link type="guide" xref="preferences#application"/>
    <link type="next" xref="preferences_table_navigation"/>
    <title type="sort">1. Navegação Web</title>
    <title type="link">Navegação web</title>
    <desc>Configurando o suporte do <app>Orca</app> para <app>Firefox</app>, <app>Thunderbird</app> e <app>Chrome</app>/<app>Chromium</app></desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Atribuição Compartilhada Igual 3.0 — Creative Commons</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Alexandre Conte</mal:name>
      <mal:email>alente.alemao@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>José Vilmar Estácio De Souza</mal:name>
      <mal:email>vilmar@informal.com.br</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Enrico Nicoletto</mal:name>
      <mal:email>liverig@gmail.com</mal:email>
      <mal:years>2013</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Leônidas Araújo</mal:name>
      <mal:email>leorusvellt@hotmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Preferências de Navegação na Web</title>
  <section id="page_navigation">
    <title>Navegação por página</title>
    <p>O grupo de controles da <gui>navegação da página</gui> possibilita que você personalize como o <app>Orca</app> apresenta textos e outros conteúdos, e como permite-lhe interagir com eles.</p>
    <section id="page_navigation_control_caret">
      <title>Controlar a navegação do cursor</title>
      <p>Esta caixa de seleção ativa e desativa a navegação de cursor do <app>Orca</app>. Quando está ativado, o <app>Orca</app> assume o controle do cursor enquanto você navega pela página; quando está desativado, a navegação de cursor nativa do navegador está ativa.</p>
      <p>Valor padrão: selecionado</p>
      <note style="tip">
        <title>Esta configuração pode ser ativada a qualquer momento</title>
        <p>Para ativar esta configuração temporariamente sem a salvar, use a <keyseq><key>Tecla Modificadora do Orca</key><key>F12</key></keyseq>.</p>
       </note>
    </section>
    <section id="page_navigation_focus_caret">
      <title>Modo de foco automático durante a navegação de cursor</title>
      <p>Se esta caixa de seleção estiver marcada, o <app>Orca</app> ativará automaticamente o modo de foco quando você usar comandos de navegação de cursor para navegar até um campo de formulário. Por exemplo, pressionar <key>Para Baixo</key> permitiria que você uma entrada, mas depois de fazer isso, o Orca alternaria para o modo de foco e os pressionamentos subsequentes de <key>Para Baixo</key> seriam controlados pelo botão navegador da web e não pelo Orca. Se esta caixa de seleção não estiver marcada, o <app>Orca</app> continuará controlando o que acontece quando você pressiona <key>Para Baixo</key>, tornando possível sair da entrada e continuar lendo.</p>
      <p>Valor padrão: não selecionado</p>
      <note style="tip">
        <title>Alternar manualmente entre o modo de navegação e o modo de foco</title>
        <p>Para iniciar ou parar de interagir com o campo de formulário focado, use <keyseq><key>Tecla Modificadora do Orca</key><key>A</key></keyseq> para alternar entre o modo de navegação e o modo de foco.</p>
       </note>
    </section>
    <section id="page_navigation_structural">
      <title>Habilitar navegação estrutural</title>
      <p>Esta caixa de seleção habilita/desabilita a <link xref="howto_structural_navigation">Navegação Estrutural</link> do <app>Orca</app>. A navegação estrutural permite-lhe navegar pelos elementos como cabeçalhos, links e campos de formulário.</p>
      <p>Valor padrão: selecionado</p>
      <note style="tip">
        <title>Esta configuração pode ser ativada a qualquer momento</title>
        <p>Para ativar esta configuração temporariamente sem a salvar, use a <keyseq><key>Tecla Modificadora do Orca</key><key>Z</key></keyseq>.</p>
       </note>
    </section>
    <section id="page_navigation_focus_structural">
      <title>Modo de foco automático durante a navegação estrutural</title>
      <p>Se esta caixa de seleção estiver marcada, o <app>Orca</app> ativará automaticamente o modo de foco quando você usar comandos de navegação estrutural para navegar para um campo de formulário. Por exemplo, pressionar <key>E</key> para mover para a próxima entrada moveria o foco para lá e também ativaria o modo de foco para que seu próximo pressionamento de <key>E</key> digite um "e" nesse entrada. Se esta caixa de seleção não estiver marcada, então o <app>Orca</app> o deixará no modo de navegação e seu próximo pressionamento de <key>E</key> o moverá para a próxima entrada na página.</p>
      <p>Valor padrão: não selecionado</p>
      <note style="tip">
        <title>Alternar manualmente entre o modo de navegação e o modo de foco</title>
        <p>Para iniciar ou parar de interagir com o campo de formulário focado, use <keyseq><key>Tecla Modificadora do Orca</key><key>A</key></keyseq> para alternar entre o modo de navegação e o modo de foco.</p>
       </note>
    </section>
    <section id="page_navigation_focus_native">
      <title>Modo de foco automático durante a navegação nativa</title>
      <p>Se esta caixa de seleção estiver marcada, o <app>Orca</app> ativará automaticamente o modo de foco quando você usar comandos de navegação do navegador nativo para passar para um campo de formulário. Por exemplo, pressionar <key>Tab</key> para mover para a próxima entrada levaria o foco para lá e também ativaria o modo de foco para que, na próxima vez que você pressionar <key>E</key> digite um "e" nesse entrada. Se esta caixa de seleção não estiver marcada, então o <app>Orca</app> o deixará no modo de navegação e seu próximo pressionamento de <key>E</key> o moverá para a próxima entrada na página.</p>
      <p>Valor padrão: selecionado</p>
      <note style="tip">
        <title>Alternar manualmente entre o modo de navegação e o modo de foco</title>
        <p>Para iniciar ou parar de interagir com o campo de formulário focado, use <keyseq><key>Tecla Modificadora do Orca</key><key>A</key></keyseq> para alternar entre o modo de navegação e o modo de foco.</p>
       </note>
    </section>
    <section id="page_navigation_speak">
      <title>Comece a falar uma página automaticamente quando ela for carregada pela primeira vez</title>
      <p>Se esta caixa de seleção estiver marcada, o <app>Orca</app> executará um Ler Tudo na página da Web ou no e-mail recém-aberto.</p>
      <p>Valor padrão: selecionado para Firefox; não selecionado para Thunderbird</p>
    </section>
    <section id="page_navigation_summary">
      <title>Apresentar resumo de uma página quando ela é carregada pela primeira vez</title>
      <p>Se esta caixa de seleção estiver marcada, o <app>Orca</app> resumirá os detalhes sobre a página da Web ou e-mail recém-aberto, como o número de títulos, pontos de referência e links.</p>
      <p>Valor padrão: selecionado para Firefox; não selecionado para Thunderbird</p>
    </section>
    <section id="page_navigation_layout_mode">
      <title>Ativar o modo de layout para conteúdo</title>
      <p>Se esta caixa de seleção estiver marcada, a navegação de cursor do <app>Orca</app> respeitará o layout do conteúdo na tela e apresentará a linha completa, incluindo quaisquer links ou campos de formulário nessa linha. Se esta caixa de seleção não estiver marcada, o <app>Orca</app> tratará objetos como links e campos de formulário como se estivessem em linhas separadas, tanto para apresentação quanto para navegação.</p>
      <p>Valor padrão: selecionado</p>
    </section>
  </section>
  <section id="table_options">
    <title>Opções de Tabela</title>
    <note>
      <p>Para saber mais sobre as opções do <app>Orca</app> para navegar em tabelas, consulte <link xref="preferences_table_navigation">Preferências de Navegação de Tabela</link>.</p>
    </note>
  </section>
  <section id="find_options">
    <title>Opções de Busca</title>
    <p>O grupo de controles das <gui>opções de busca</gui> permite-lhe personalizar como o <app>Orca</app> apresenta o resultado de uma pesquisa realizada com as funcionalidades de busca próprias do aplicativo.</p>
    <section id="find_options_speak_during_find">
      <title>Falar resultados durante a busca</title>
      <p>Se esta caixa de seleção for marcada, o <app>Orca</app> lerá a linha que combina com a sua pesquisa atual.</p>
      <p>Valor padrão: selecionado</p>
    </section>
    <section id="find_options_speak_changed_lines_during_find">
      <title>Somente falar as linhas alteradas durante a pesquisa</title>
      <p>Se esta caixa de seleção estiver marcada, o <app>Orca</app> não apresentará a linha correspondente se for a mesma linha da correspondência anterior. Essa opção foi projetada para evitar "chattiness" em uma linha com várias instâncias da string que você está procurando.</p>
      <p>Valor padrão: não selecionado</p>
    </section>
    <section id="find_options_minimum_match_length">
      <title>Tamanho mínimo de textos encontrados</title>
      <p>Este botão de rotação editável é onde você pode especificar o número de caracteres correspondentes antes que o <app>Orca</app> anuncie a linha correspondente. Esta opção também foi projetada para evitar "excesso de fala" caso existam muitas ocorrências quando você começa a digitar o texto que você está procurando.</p>
      <p>Valor padrão: 4</p>
    </section>
  </section>
</page>
