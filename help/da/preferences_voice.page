<?xml version="1.0" encoding="utf-8"?>
<page xmlns="http://projectmallard.org/1.0/" type="topic" id="preferences_voice" xml:lang="da">
  <info>
    <link type="guide" xref="preferences#orca"/>
    <link type="next" xref="preferences_speech"/>
    <title type="sort">2.0 Stemme</title>
    <title type="link">Stemme</title>
    <desc>Konfiguration af den stemme, som bruges af <app>Orca</app></desc>
    <credit type="author">
      <name>Joanmarie Diggs</name>
      <email>joanied@gnome.org</email>
    </credit>
    <license>
      <p>Creative Commons Deling på samme vilkår 3.0</p>
    </license>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>scootergrisen</mal:name>
      <mal:email/>
      <mal:years>2020-2022&lt;</mal:years>
    </mal:credit>
  
    <mal:credit xmlns:mal="http://projectmallard.org/1.0/" type="translator copyright">
      <mal:name>Ask Hjorth Larsen</mal:name>
      <mal:email>asklarsen@gmail.com</mal:email>
      <mal:years>2022</mal:years>
    </mal:credit>
  </info>
  <title>Indstillinger for stemme</title>
  <section id="voice_type_settings">
    <title>Indstillinger for stemmetype</title>
    <section id="voice_type">
      <title>Stemmetype</title>
      <p>Kombinationsboksen giver dig mulighed for at bruge forskellige stemmer, så du bedre kan skelne store bogstaver og linket tekst fra anden tekst, og tekst på skærmen fra tekst, som er tilføjet af <app>Orca</app>.</p>
      <note style="tip">
        <title>Konfiguration af flere stemmer</title>
        <p>For hver stemme, du ønsker at konfigurere, vælges først stemmen i kombinationsboksen <gui>Stemmetype</gui>. Konfigurer så den person, hastighed, tonehøjde og lydstyrke, som skal bruges til stemmen.</p>
      </note>
    </section>
    <section id="speech_system">
      <title>Talesystem</title>
      <p>Kombinationsboksen giver dig mulighed for at vælge dit foretrukne talesystem blandt dem, du har installeret, såsom Speech Dispatcher.</p>
    </section>
    <section id="speech_synthesizer">
      <title>Talesynthesizer</title>
      <p>Kombinationsboksen giver dig mulighed for at vælge den talesynthesizer, som skal bruges med det talesystem, du har valgt.</p>
    </section>
    <section id="person">
      <title>Person</title>
      <p>Kombinationsboksen giver dig mulighed for at vælge hvilken “person” eller “taler”, der skal bruges med den valgte stemme. Det kan f.eks. være, du ønsker, at David skal tale som standard men have hyperlinks udtalt af Alice. Bemærk, at det du finder i kombinationsboksen <gui>Person</gui> afhænger af hvilke talesynthesizere, du har installeret.</p>
    </section>
    <section id="capitalization_style">
      <title>Stil for ord med stort</title>
      <p>Kombinationsboksen giver dig mulighed for at vælge hvilke indikationsstile for ord med stort, du ønsker at bruge til Speech Dispatcher, ud over <app>Orca</app>s stemme for ord med stort. Indstillingerne, som er navngivet med Speech Dispatchers terminologi, er:</p>
     <list>
       <item><p><gui>Ikon</gui>: Afspiller en tone</p></item>
       <item><p><gui>Stav</gui>: Siger ordet “med stort”</p></item>
       <item><p><gui>Ingen</gui></p></item>
    </list>
    <note style="tip">
      <title>Indstillingen kan slås til/fra løbende</title>
      <p><app>Orca</app> har også en kommando til at skifte mellem de tilgængelige stile for ord med stort. Se <link xref="commands_speech_settings">Kommandoer for tale</link> for mere information.</p>
    </note>
    <p>Standardværdi: ingen</p>
    </section>
    <section id="rate_pitch_and_volume">
      <title>Hastighed, tonehøjde og lydstyrke</title>
      <p>Disse tre venstre-højre-skydere giver dig mulighed for at tilpasse lyden på den person, du lige har valgt.</p>
    </section>
  </section>
  <section id="global_voice_settings">
  <title>Globale indstillinger for stemme</title>
    <section id="pauses">
      <title>Opdel tale i bidder mellem pauser</title>
      <p>Afhængig af hvilke taleindstillinger som er aktiveret, kan <app>Orca</app> have ret så meget at sige om et bestemt objekt såsom dets navn, dets rolle, dets tilstand, dets genvejstast, dets vejledningsmeddelelse osv. Tilvalg af afkrydsningsboksen <gui>Opdel tale i bidder mellem pauser</gui> får <app>Orca</app> til at indsætte korte pauser mellem hvert stykke information.</p>
      <p>Standardværdi: tilvalgt</p>
    </section>
    <section id="multicase_strings">
      <title>Oplæs strenge med en blanding af store og små bogstaver som ord</title>
      <p>I noget tekst, særligt i programmering, kan man ofte støde på et “ord”, som består af flere ord med skiftende store/små bogstaver såsom “StoreSmåBogstaver”. Talesynthesizere udtaler ikke altid disse strenge korrekt. Tilvælges afkrydsningsboksen <gui>Oplæs strenge med en blanding af store og små bogstaver som ord</gui>, vil det få <app>Orca</app> til at opdele et ord som “StoreSmåBogstaver” i separate ord (“Store”, “Små” og “Bogstaver”) inden, de videregives til talesynthesizeren.</p>
      <p>Standardværdi: fravalgt</p>
    </section>
    <section id="numbers_as_digits">
      <title>Oplæs tal som cifre</title>
      <p>Tilvalg af afkrydsningsboksen <gui>Oplæs tal som cifre</gui> får <app>Orca</app> til at opdele et tal såsom “123” i separate cifre (“1”, “2” og “3”) inden, de videregives til talesynthesizeren.</p>
      <p>Standardværdi: fravalgt</p>
    </section>
  </section>
</page>
